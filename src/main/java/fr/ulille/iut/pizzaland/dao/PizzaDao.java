package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL); ")
    	    void createPizzaTable();

    	    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS ingredientsPizza "
    		+ "( idIngredients VARCHAR(128), "
    		+ "idPizzas VARCHAR(128), "
    		+ "FOREIGN KEY(idIngredients) REFERENCES ingredients(id), "
    		+ "FOREIGN KEY(idPizzas) REFERENCES pizzas(id), "
    		+ "PRIMARY KEY (idPizzas,idIngredients))  ")
	void createAssociationTable();
    

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTablePizza();
    
    @SqlUpdate("DROP TABLE IF EXISTS ingredientsPizza")
    void dropTableIngredientsPizza();
    
    @Transaction
	default void createTablePizzaAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropTableIngredientsPizza();
		dropTablePizza();
	}

	@Transaction
	default void deletePizzasByID(UUID id) {
		removeIngredientsPizza(id);
		removePizza(id);
	}

	@Transaction
	default void insertIntoPizzas(Pizza pizza) {
		insertPizzas(pizza);
		for (Ingredient ingredient : pizza.getIngredients()) {
			insertAssociation(pizza.getId(),ingredient.getId());
		}    	
	}
	
	@Transaction
	default Pizza getFromId(UUID id) {
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		Pizza p = findById(id);
		List<UUID> idList= findByIdIngredient(id);
		for(UUID idI : idList) {
			p.addOneIngredient(ingredients.findById(idI));
		}
		return p;
	}

    @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);
    
    @SqlUpdate("DELETE FROM ingredientsPizza WHERE idP = :id")
	void removeIngredientsPizza(@Bind("id") UUID id);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void removePizza(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT idI FROM ingredientsPizza WHERE idP = :id")
	List<UUID> findByIdIngredient(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
	void insertPizzas(@BindBean Pizza pizzas);
    

	@SqlUpdate("INSERT INTO ingredientsPizza (idPizzas, idIngredients) VALUES (:idPizzas, :idIngredients)")
	void insertAssociation(@Bind("idP") UUID idP, @Bind("idI") UUID idI);
    
    
    
}