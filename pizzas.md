
| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | Liste des pizzas (I2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | Une pizza (I2) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | Le nom de la pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Ingrédient (I1) | Nouvelle pizza (I2)<br>409 si l'ingrédient existe déjà (même nom) |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |




Une pizza comporte uniquement un identifiant, un nom et une liste d'ingrédient(s). Sa représentation JSON (P3) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "margherita"
      "ingredients": [{"id":"c77deeee-d50d-49d5-9695-c98ec811f762","name":"tomate"},
                      {"id":"657f8dd4-6bc1-4622-9af7-37d248846a23","name":"fromage"}]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingrédient. Aussi on aura une
représentation JSON (P2) qui comporte uniquement le nom :

    {
        "name": "margherita"
        "ingredients": [{"id":"c77deeee-d50d-49d5-9695-c98ec811f762","name":"tomate"},
                        {"id":"657f8dd4-6bc1-4622-9af7-37d248846a23","name":"fromage"}]
    }

Ou bien sous sa représentation JSON (P1) :

     {
          "name": "margherita"
     }
   